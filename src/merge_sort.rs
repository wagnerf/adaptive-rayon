//! different merge sort implementations.
use itertools::kmerge;
use rayon;

/// Parallel recursive merge sort
pub fn merge_sort<T: Ord + Copy + Send>(slice: &mut [T]) {

    let n = slice.len();
    if n <= 1 {
        return;
    }

    let mut fused: Vec<T> = Vec::with_capacity(n);
    unsafe {
        fused.set_len(n);
    }
    {
        let (slice1, slice2) = slice.split_at_mut(n / 2);
        rayon::join(|| merge_sort(slice1), || merge_sort(slice2));
        merge(slice1, slice2, &mut fused);
    }
    for (source, destination) in fused.iter().zip(slice.iter_mut()) {
        *destination = *source;
    }
}

fn merge<T: Ord + Copy>(input_slice1: &[T], input_slice2: &[T], output: &mut [T]) {
    for (source, destination) in
        kmerge(vec![input_slice1.iter(), input_slice2.iter()]).zip(output.iter_mut())
    {
        *destination = *source;
    }
}

//#[test]
#[cfg(test)]
mod test {
    use ARRAY_SIZE;
    use super::merge_sort;
    use itertools::repeat_call;
    use rand;

    #[test]
    fn test_merge_sort_recursive_parallel() {
        let mut v: Vec<_> = repeat_call(rand::random::<i32>).take(ARRAY_SIZE).collect();
        merge_sort(&mut v);
        for w in v.windows(2) {
            assert!(w[0].le(&w[1]));
        }
    }
}

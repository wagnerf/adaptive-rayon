use rayon;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc::channel;
use std::sync::Arc;

pub fn parallel_increment<T: Send, F: Fn(&T) -> T + Sync>(
    slice: &mut [T],
    op: &F,
    micro_blocks_size: usize,
) {
    let stolen = Arc::new(AtomicBool::new(false));
    let stealing = stolen.clone();
    let (tx, rx) = channel();

    rayon::join(
        move || {
            let achieved = slice
                .chunks_mut(micro_blocks_size)
                .take_while(|_| !stolen.load(Ordering::Relaxed))
                .map(|s| sequential_increment(s, op))
                .count();
            let next_start = achieved * micro_blocks_size;
            if next_start >= slice.len() {
                tx.send(None).expect("sending no work failed");
            } else {
                let (_, remaining) = slice.split_at_mut(next_start);
                let half = remaining.len() / 2;
                let (my_work, given_work) = remaining.split_at_mut(half);
                tx.send(Some(given_work)).expect("sending work failed");
                parallel_increment(my_work, op, micro_blocks_size);
            }
        },
        move || {
            stealing.store(true, Ordering::Relaxed);
            rx.recv()
                .expect("receiving work failed")
                .map(|s| parallel_increment(s, op, micro_blocks_size))
        },
    );
}

pub fn sequential_increment<T, F: Fn(&T) -> T>(slice: &mut [T], op: &F) {
    for e in slice {
        *e = op(e);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::iter::repeat;
    use test::Bencher;
    use {ARRAY_SIZE, MICRO_SIZE};
    use rayon::prelude::*;

    fn rayon_parallel_increment<T: Send, F: Fn(&T) -> T + Sync>(
        slice: &mut [T],
        op: &F,
        micro_blocks_size: usize,
    ) {
        slice.par_chunks_mut(micro_blocks_size).for_each(
            |s| for e in s {
                *e = op(e)
            },
        );
    }

    #[bench]
    fn sequential_add_int(b: &mut Bencher) {
        let mut v: Vec<_> = repeat(1).take(ARRAY_SIZE).collect();
        b.iter(|| sequential_increment(&mut v, &|x| *x + 1))
    }

    #[bench]
    fn sequential_mul_float(b: &mut Bencher) {
        let mut v: Vec<_> = repeat(1.0).take(ARRAY_SIZE).collect();
        b.iter(|| sequential_increment(&mut v, &|x| *x * 2.0))
    }

    #[bench]
    fn parallel_add_int(b: &mut Bencher) {
        let mut v: Vec<_> = repeat(1).take(ARRAY_SIZE).collect();
        b.iter(|| parallel_increment(&mut v, &|x| *x + 1, MICRO_SIZE))
    }

    #[bench]
    fn parallel_mul_float(b: &mut Bencher) {
        let mut v: Vec<_> = repeat(1.0).take(ARRAY_SIZE).collect();
        b.iter(|| parallel_increment(&mut v, &|x| *x * 2.0, MICRO_SIZE))
    }

    #[bench]
    fn rayon_parallel_add_int(b: &mut Bencher) {
        let mut v: Vec<_> = repeat(1).take(ARRAY_SIZE).collect();
        b.iter(|| rayon_parallel_increment(&mut v, &|x| *x + 1, MICRO_SIZE))
    }

    #[bench]
    fn rayon_parallel_mul_float(b: &mut Bencher) {
        let mut v: Vec<_> = repeat(1.0).take(ARRAY_SIZE).collect();
        b.iter(|| {
            rayon_parallel_increment(&mut v, &|x| *x * 2.0, MICRO_SIZE)
        })
    }
}

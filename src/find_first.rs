use rayon;
use std::sync::mpsc::{channel, Receiver};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

pub fn adaptive_find<T: Eq + Send + Sync>(slice: &[T], target: &T) -> Option<usize> {
    let macro_size = 10_000_000; // TODO: pas adaptif pour garantir un %age perdu ?
    slice
        .chunks(macro_size)
        .enumerate()
        .filter_map(|(i, s)| parallel_find(s, target, i * macro_size))
        .next()
}

fn parallel_find<T: Eq + Send + Sync>(slice: &[T], target: &T, offset: usize) -> Option<usize> {
    let work_requested = Arc::new(AtomicBool::new(false));
    let requesting_work = work_requested.clone();
    let (tx, rx) = channel();

    let (result, result2) = rayon::join(
        move || {
            let micro_size = 1000;
            let mut next_block = 0;
            let found = slice
                .chunks(micro_size)
                .take_while(|_| !work_requested.load(Ordering::Relaxed))
                .enumerate()
                .filter_map(|(i, s)| {
                    next_block = i + 1;
                    sequential_find(s, target)
                })
                .next();

            if let Some(position) = found {
                // completed with finding ; no steal request
                tx.send(None).expect("sending no work failed");
                Some(offset + position + (next_block - 1) * micro_size)
            } else {
                let next_start = next_block * micro_size;
                if next_start >= slice.len() {
                    // completed without finding ; no steal request
                    tx.send(None).expect("sending no work failed");
                    None
                } else {
                    // split remaining work
                    assert!(work_requested.load(Ordering::Relaxed));
                    let (_, remaining) = slice.split_at(next_start);
                    let half = remaining.len() / 2;
                    let (kept, given) = remaining.split_at(half);
                    tx.send(Some((given, offset + next_start + half)))
                        .expect("sending work failed");
                    parallel_find(kept, target, offset + next_start)
                }
            }
        },
        move || steal_find(&rx, target, requesting_work),
    );
    result.or(result2)
}

fn steal_find<T: Eq + Send + Sync>(
    rx: &Receiver<Option<(&[T], usize)>>,
    target: &T,
    requesting_work: Arc<AtomicBool>,
) -> Option<usize> {
    requesting_work.store(true, Ordering::Relaxed);
    if let Some((slice, offset)) = rx.recv().expect("receiving failed") {
        parallel_find(slice, target, offset)
    } else {
        None
    }
}

pub fn sequential_find<T: Eq>(slice: &[T], target: &T) -> Option<usize> {
    slice.iter().position(|e| *e == *target)
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand;
    use test::Bencher;
    use ARRAY_SIZE;
    use rayon::prelude::*;

    #[test]
    fn random_array() {
        let v: Vec<_> = (0..5_000_000)
            .into_iter()
            .map(|_| rand::random::<i32>() % 1000)
            .collect();
        let position = rand::random::<usize>() % v.len();
        let target = v[position];
        let found_position = sequential_find(&v, &target).unwrap();
        assert!(found_position <= position);
        assert_eq!(found_position, parallel_find(&v, &target, 0).unwrap());
        assert_eq!(found_position, adaptive_find(&v, &target).unwrap());
    }


    #[bench]
    fn sequential(b: &mut Bencher) {
        let v: Vec<_> = (0..ARRAY_SIZE)
            .into_iter()
            .map(|_| rand::random::<i32>())
            .collect();
        let position = v.len() - 1;
        let target = v[position];
        b.iter(|| {
            let found_position = sequential_find(&v, &target).unwrap();
            assert!(found_position <= position);
        })
    }

    #[bench]
    fn parallel(b: &mut Bencher) {
        let v: Vec<_> = (0..ARRAY_SIZE)
            .into_iter()
            .map(|_| rand::random::<i32>())
            .collect();
        let position = v.len() - 1;
        let target = v[position];
        b.iter(|| {
            let found_position = parallel_find(&v, &target, 0).unwrap();
            assert!(found_position <= position);
        })
    }

    #[bench]
    fn rayon(b: &mut Bencher) {
        let v: Vec<_> = (0..ARRAY_SIZE)
            .into_iter()
            .map(|_| rand::random::<i32>())
            .collect();
        let position = v.len() - 1;
        let target = v[position];
        b.iter(|| {
            let found_position = v.par_iter().position_first(|e| *e == target).unwrap();
            assert!(found_position <= position);
        })
    }

    #[bench]
    fn adaptive(b: &mut Bencher) {
        let v: Vec<_> = (0..ARRAY_SIZE)
            .into_iter()
            .map(|_| rand::random::<i32>())
            .collect();
        let position = v.len() - 1;
        let target = v[position];
        b.iter(|| {
            let found_position = adaptive_find(&v, &target).unwrap();
            assert!(found_position <= position);
        })
    }
}

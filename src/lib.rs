#![feature(test)]
extern crate rayon;
extern crate rand;
extern crate time;
extern crate test;
extern crate itertools;
pub mod increment;
pub mod prefix;
pub use prefix::Prefix;
pub mod find_first;
pub mod merge_sort;

// some benchmarks parameters
#[cfg(test)]
const ARRAY_SIZE: usize = 2_000_000;
#[cfg(test)]
const MACRO_SIZE: usize = 1_000_000;
#[cfg(test)]
const MICRO_SIZE: usize = 30;

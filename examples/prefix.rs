extern crate adaptive_rayon;
extern crate time;
use adaptive_rayon::Prefix;
use std::iter::repeat;
use time::now;

fn bench(array_size: usize, macro_blocks_size: usize, micro_blocks_size: usize) {
    let mut v: Vec<_> = repeat(1.0).take(array_size).collect();
    //let mut v: Vec<_> = repeat(1).take(array_size).collect();
    let start = now();
    Prefix::new(&mut v, &|a, b| *a * *b)
    //Prefix::new(&mut v, &|a, b| *a + *b)
        .macro_size(macro_blocks_size)
        .micro_size(micro_blocks_size)
        .run();
    println!("{} {} {} {}",
             array_size,
             macro_blocks_size,
             micro_blocks_size,
             (now() - start).num_microseconds().unwrap());
    //for (i, e) in v.iter().enumerate() {
    //    assert_eq!(*e, i + 1);
    //}
    for e in &v {
        assert_eq!(*e, 1.0);
    }
}

fn main() {
    println!("array_size macro_blocks_size micro_blocks_size prefix_run_microseconds");
    for _ in 1..100 {
        for array_size in &[5_000_000, 10_000_000, 20_000_000] {
            for macro_blocks_size in &[500_000, 1_000_000, 2_000_000, 5_000_000] {
                for micro_blocks_size in &[10, 20, 30, 50, 100, 200, 500] {
                    bench(*array_size, *macro_blocks_size, *micro_blocks_size);
                }
            }
        }
    }
}

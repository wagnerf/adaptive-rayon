
extern crate itertools;
extern crate rand;
extern crate adaptive_rayon;
use adaptive_rayon::merge_sort::merge_sort;
use itertools::repeat_call;

fn main() {
    let mut v: Vec<_> = repeat_call(rand::random::<i32>).take(62500).collect();
    merge_sort(&mut v);
    //println!("{:?}", v);
    for w in v.windows(2) {
        assert!(w[0].le(&w[1]));
    }
}
